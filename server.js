const schedule = require("node-schedule");
const express = require("express");
const app = express();

const PORT = 6542;

var date1 = new Date(2020, 11, 13, 08, 50, 0);
var date2 = new Date(2020, 11, 13, 08, 50, 0);

var job1 = null,
  job2 = null,
  job3 = null;

var count = 0;

const arrayStud = [
  {
    name: "student1",
  },
  {
    name: "student2",
  },
  {
    name: "student3",
  },
];

const job = schedule.scheduleJob("*/10 * * * * *", () => {
  let now = new Date();

  const twoHours = 60 * 60 * 2 * 1000;
  const oneDay = 60 * 60 * 24 * 1000;

  // DAY 1 24HOUR MAIL
  let day1 = new Date(date1);
  var day1_Difference = day1 - now < oneDay;
  if (day1_Difference) {
    // SEND DAY 1 24HOUR EMAIL
    // CREATE A CHECK FOR STUDENT DAY 1 MAIL SENT
  }

  // DAY 2 24HOUR MAIL
  let day2 = new Date(date2);
  var day2_Difference = day2 - now < oneDay;
  if (day2_Difference) {
    // SEND DAY 2 24HOUR EMAIL
    // CREATE A CHECK FOR STUDENT DAY 2 MAIL SENT
  }

  // DAY 2 2HOUR MAIL
  var day2TwoHour_Difference = day2 - now < twoHours;
  if (day2TwoHour_Difference) {
    // SEND DAY 2 2HOUR EMAIL
    // CREATE A CHECK FOR STUDENT DAY 2 2HOUR MAIL SENT
  }
});

app.listen(PORT, () => console.log(`Server running at port: ${PORT}`));
